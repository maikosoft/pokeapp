import React, {Component} from 'react';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
      message: "",
      pokemons: []
    }
    this.changeName = this.changeName.bind(this);
    this.searchPokemon = this.searchPokemon.bind(this);
  }
  changeName(event) {
    this.setState({message: ""});
    this.setState({name: event.target.value});
  }
  searchRepeated(nameToSearch) {
    let pokemonList = this.state.pokemons;
    let result = false;
    for (let i = 0; i < pokemonList.length; i++) {      
      if(pokemonList[i].name == nameToSearch) {
        result = true;
        break;
      }
    }
    return result;
  }
  searchPokemon(event) {
    event.preventDefault();
    let nameToSearch = this.state.name.toLowerCase();
    fetch('https://pokeapi.co/api/v2/pokemon/' + nameToSearch  + '/')
    .then(res => {
      if(res.ok){
        return res.json();
      } else {
        return res = null;
      }
    })
    .then((data) => {
      if(data !== null) {
        let pokemonList = this.state.pokemons;
        if(this.searchRepeated(nameToSearch) === false) {
          pokemonList.push({
            id: data.id,
            name: data.name,
            image: data.sprites.front_default
          });
          this.setState({ pokemons: pokemonList });
        } else {
          this.setState({message: "Allready in the list"});
        }
        console.log(this.state.pokemons);
      } else {
        this.setState({message: "Not Found"});
      }
    })
    .catch(error => {
      this.setState({message: "Error, Not Found"});
    })
  }
  
  render () {
   
    let listOfPokemons = [];
    if(this.state.pokemons.length > 0) {
      this.state.pokemons.forEach(pokemon => {
        listOfPokemons.push(
          <div>
            <div class="Div-inline">{pokemon.id}</div>
            <div class="Div-inline"><img src={pokemon.image} /></div>
            <div class="Div-inline">{pokemon.name}</div>
          </div>);
      });
      
    } else {
      listOfPokemons.push(<div><p>No Pokemons captured yet</p></div>);
    }
    return (
      <div className="App">
        <header className="App-header">
          <h4>Pokemon App</h4>
        </header>
        <section>
            <form onSubmit={this.searchPokemon}>
              <input type="text" value={this.state.name} placeholder="Pokemon Name" onChange={this.changeName}/>
              <button type="submit">Catch</button>
            </form>
        </section>
        <section>
          <p className="App-error">{this.state.message}</p>
        </section>
        <section>
            <h3>Pokemon List:</h3>
            {listOfPokemons}
        </section>

      </div>
    );
  }
}

export default App;
